import { TweenMax, TimelineMax } from 'gsap'
import { qs, qsa } from '../lib/utils'
import { Expo, Power2 } from 'gsap/EasePack'
import SplitText from '../lib/SplitText'

const animations = new Map()

animations.set('setimage', (el) => {
  TweenMax.set(el, {
    autoAlpha: 0,
    scale: 1.2
  })
})

animations.set('image', (el, delay = 0) => {

    TweenMax.to(el, 1, {
      autoAlpha: 1,
      scale: 1,
      ease: Power2.easeOut
    })
})

animations.set('settext', (el) => {
  TweenMax.set(el, {
    autoAlpha: 0
  })
})

animations.set('text', (el, delay = 0) => {
  const tl = new TimelineMax({delay})
  const split = new SplitText(el, {type: 'words,chars'})

  tl
    .set(split.chars, {
      autoAlpha: 0,
      x: 16
    })
    .set(el, {
      autoAlpha: 1
    })
    .staggerTo(split.chars, 1, {
      autoAlpha: 1,
      x: 0,
      ease: Expo.easeOut
    }, 0.02)
    .add('finish')
    .add(() => {split.revert()}, 'finish+=0.5')
})

animations.set('setlink', (el) => {
  TweenMax.set(el, {
    autoAlpha: 0
  })
})

animations.set('link', (el, delay = 0) => {
  TweenMax.to(el, 1, {
    autoAlpha: 1,
    delay
  })
})


export default animations